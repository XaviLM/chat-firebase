export interface Mensaje {
    nombre: String;
    mensaje: String;
    email?: String;
    fecha?: number;
    uid?: string;
}
