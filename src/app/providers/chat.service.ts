import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Mensaje } from '../interfaces/mensaje.interface';


import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  public chats: Mensaje[] = [];
  public usuario: any = {};


  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      console.log('Estado del usuario', user);
      if (!user) {
        return;
      }
      this.usuario.uid = user.uid;
      if (user.isAnonymous) {
        let text = user.uid.substr(-5);
        this.usuario.nombre = 'Anónimo-' + text;
        this.usuario.photo = 'assets/img/anon.jpg';
        this.usuario.email = 'anonimous@anonimous.chat';
      } else {
        this.usuario.nombre = user.displayName;
        this.usuario.photo = user.photoURL;
        this.usuario.email = user.email;
      }

    });
  }

  login(proveedor: string) {
    if (proveedor === 'google') {
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    } else if (proveedor === 'twitter') {
      this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider());
    } else if (proveedor === 'anon') {
      this.afAuth.auth.signInAnonymously().catch(function (error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        console.error(errorCode, errorMessage);
        // ...
      });
    }

  }
  logout() {
    this.afAuth.auth.signOut();
    this.usuario = {};
  }


  cargarMensajes() {
    let ahora = new Date().getTime();
    console.log('hoy: ', ahora);
    let ayer = ahora - (24 * 86400);
    console.log('ayer: ', ayer);
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'desc')
      .limit(5));
    return this.itemsCollection.valueChanges()
      .pipe(map((mensajes: Mensaje[]) => {
        console.log('cargar estos mensajes', mensajes);
        this.chats = [];
        for (const mensaje of mensajes) {
          this.chats.unshift(mensaje);
          if (mensaje.fecha < ayer) {
            console.log('Mensaje anterior a ayer');
            this.borrar();
          }
        }
        return this.chats;
      }));
  }

  agregarMensaje(texto: string) {
    // falta UID del user
    const mensaje: Mensaje = {
      nombre: this.usuario.nombre,
      uid: this.usuario.uid,
      mensaje: texto,
      fecha: new Date().getTime()
    };

    return this.itemsCollection.add(mensaje);
  }
  borrar() {
    // this.afs.database.object('/--your-list-name--/--your-obejct-key--').remove();
    // this.afs.c .app['chats'].delete();
    let storedData = [];
    this.afs.collection('chats').get()
      .subscribe(data => {
        console.log('borrar esto:', data.docs);
        if (data.docs) {
          storedData = data.docs;
          for (let j = 0; j < storedData.length; j++) {
            console.log('borrar esto:', storedData[j].id);
            this.afs.collection('chats').doc(storedData[j].id).delete();
          }
        }
      });

    // return this.afs.collection('chats').doc('SZKQSY6dR20LSgkFQxdZ').delete();
  }


}
