
# Chat-firebase

Chat developed with __Angular 7__ and __Google Firebase__.



# installed Packages
## Bootstrap - JQuery - PopperJS

```javascript
npm install bootstrap jquery popper.js --save
```

## Font-awesome

```javascript
npm install font-awesome
```
## AngularFire module and Firebase

```
npm install @angular/fire firebase --save
```
